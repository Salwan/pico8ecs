---------------- TODOs
-- Nothin!

---------------- Love2D-PICO8 adaptor
function add(t, v)
	table.insert(t, v)
end
function del(t, v)
	table.remove(t, v)
end

---------------- Main

function _init()
end

function _update60()
	UpdateECS()
end

function _draw() 
	DrawECS()
end

--------------------- ECS
-- ecsEntity: eid = {comps = {comps_list}, entity = { {comps_data}, .. } }
-- ecsComponent: { name = { data }, .. }
-- ecsSystem: { {proc, ent_bucket}, .. }
-- ecsBucketsList: { bucket_id = comps_list, .. }
-- ecsBucket: { bucket_id = {ent0id, ent1id, ..}, bucket_id = {ent2id, ent5id, ..}, .. }
ecsEntityId = 1
ecsBucketId = 1
ecsEntities = {}
ecsComponents = {}
ecsUSystems = {}
ecsDSystems = {}
ecsBucketsList = {}
ecsBuckets = {} 

function ecsNextEntityId()
	ecsEntityId = ecsEntityId + 1
	return ecsEntityId - 1
end

function ecsNextBucketId()
	ecsBucketId = ecsBucketId + 1
	return ecsBucketId - 1
end 

-- Returns existing bucket or new one
function ecsGetBucket(comps_list)
	local i, bl, found 
	local buckid = nil
	for i, c in pairs(ecsBucketsList) do 
		if ecsCompEq(comps_list, c) then 
			buckid = i
			break
		end 
	end 
	if buckid == nil then 
		buckid = ecsNextBucketId()
		ecsBuckets[buckid] = {}
		ecsBucketsList[buckid] = comps_list
	end
	return ecsBuckets[buckid]
end

-- Get all compatible buckets to passed components list
function ecsGetCompatBuckets(comps_list)
	local buckids = {}
	for i, c in pairs(ecsBucketsList) do 
		-- entity components (comps_list) must provide what bucket needs (c)
		-- entity "pos"  incompatible with bucket "pos", "sprite"
		if ecsCompIn(c, comps_list) then 
			add(buckids, i) 
		end
	end 
	return buckids
end 

-- Compare two component lists for exact order match (does not work for different order)
function ecsCompEqOrd(comp1, comp2)
	if #comp1 ~= #comp2 then return false end 
	for i = 1, #comp1 do
		if comp1[i] ~= comp2[i] then 
			return false 
		end 
	end 
	return true
end 

-- Compare two component lists for unordered match (more usable and expensive)
function ecsCompEq(comp1, comp2)
	if #comp1 ~= #comp2 then return false end 
	local found
	for i = 1, #comp1 do 
		found = false 
		for j = 1, #comp2 do 
			if comp1[i] == comp2[j] then 
				found = true
				break
			end
		end
		if found == false then return false end
	end 
	return true
end

-- Returns true if given component name is already defined as a component or not
function ecsCheckComp(comp)
	for k, v in pairs(ecsComponents) do 
		if comp == k then 
			return true 
		end
	end
	return false
end

-- Shows an error indicating that given comp is not defined
function ecsAssertComps(comps)
	local i, comp
	for i = 1, #comps do  
		comp = comps[i]
		if ecsCheckComp(comp) == false then
			local msg = "Component \"" .. comp .. "\" is not defined!"
			print(msg)
		end
	end
end

function ecsCreateComp(comp)
	local newcomp = {}
	for i, v in pairs(comp) do 
		newcomp[i] = v 
	end
	return newcomp
end 

-- Return true if subset is in set (bit expensive)
function ecsCompIn(subset, set)
	if #subset > #set then return false end
	local i, j, found
	found = 0
	for i = 1, #subset do 
		for j = 1, #set do 
			if subset[i] == set[j] then
				found = found + 1
				break
			end 
		end
	end
	return found == #subset
end 

function ecsExecSystem(system)
	if system == nil then return end
	local i, eid
	for i, eid in pairs(system.ent_bucket) do
		system.proc(ecsEntities[eid].entity, eid)
	end
end

function ecsExecSystems(systems)
	local i, s
	for i, s in pairs(systems) do 
		ecsExecSystem(s)
	end
end

function UpdateECS()
	ecsExecSystems(ecsUSystems) 
end 

function DrawECS()
	ecsExecSystems(ecsDSystems)
end

function DefineComponent(name, comp_data) 
	if comp_data == nil then 
		print("Component " .. name .. " needs comp_data description!")
	end
	ecsComponents[name] = comp_data
end 

function DefineUpdateSystem(comps_list, system_proc)
	ecsAssertComps(comps_list)
	local bucket = ecsGetBucket(comps_list)
	add(ecsUSystems, {proc = system_proc, ent_bucket = bucket})
end 

function DefineDrawSystem(comps_list, system_proc)
	ecsAssertComps(comps_list)
	local bucket = ecsGetBucket(comps_list)
	add(ecsDSystems, {proc = system_proc, ent_bucket = bucket})
end

function SpawnEntity(comps_list)
	ecsAssertComps(comps_list)
	local eid = ecsNextEntityId()
	local entity_data = {} 
	-- Create actual components in entity_data 
	local i
	for i = 1, #comps_list do
		entity_data[comps_list[i]] = ecsCreateComp(ecsComponents[comps_list[i]])
	end 
	ecsEntities[eid] = {
		comps = comps_list,
		entity = entity_data
	}
	local cbucks = ecsGetCompatBuckets(comps_list)
	for i = 1, #cbucks do
		add(ecsBuckets[cbucks[i]], eid)
	end 
	return eid, entity_data
end 

function GetEntity(eid)
	return ecsEntities[eid].entity
end

function KillEntity(eid)
	if ecsEntities == nil or ecsEntities[eid] == nil then return end
	local i, bs, j, b
	for i, bs in pairs(ecsBuckets) do
		for j, b in pairs(bs) do 
			if eid == b then 
				del(bs, eid)
				break
			end
		end
	end
	del(ecsEntities, eid)
end 

function KillAllEntities()
	-- Resets all entity buckets, and entities 
	ecsBuckets = {} 
	ecsBucketsList = {} 
	ecsEntities = {}
end 

-- resets everything to init
function ecsHardReset()
	ecsEntityId = 1
	ecsBucketId = 1
	ecsEntities = {}
	ecsComponents = {}
	ecsUSystems = {}
	ecsDSystems = {}
	ecsBucketsList = {}
	ecsBuckets = {} 
end

----------------- Define components

CPos = {
	x = 0,
	y = 0
}

CSprite = {
	frame = 1,
	flip_x = false,
	flip_y = false,
}

CPlayer = {
	mode = "play",
}

CRect8 = {
	color = {0,0,255,255}
}

DefineComponent("pos", CPos)
--DefineComponent("sprite", CSprite)
--DefineComponent("player", CPlayer)
DefineComponent("rect8", CRect8)

----------------- Define systems

USPlayerController = function(ent)
	if btn(1) then ent.pos.x = ent.pos.x + 1 end 
	if btn(0) then ent.pos.x = ent.pos.x - 1 end 
end 

DSSpriteDrawer = function(ent)
	spr(ent.sprite.frame, ent.pos.x, ent.pos.y, 1, 1, ent.sprite.flip_x, ent.sprite.flip_y)
end 

DSRectDrawer = function(ent, eid)
	love.graphics.setColor(ent.rect8.color)
	love.graphics.rectangle("fill", ent.pos.x, ent.pos.y, 64, 64)
	love.graphics.setColor({255,255,255,255})
end

--DefineUpdateSystem({"player", "pos"}, USPlayerController)
--DefineDrawSystem({"sprite"}, DSSpriteDrawer)
DefineDrawSystem({"pos", "rect8"}, DSRectDrawer)

----------------- Create entities

--eid = SpawnEntity({"pos", "sprite", "player"})
SpawnEntity({"pos", "rect8"})

----------------- Clearing entities 

--KillEntity(eid)
--KillAllEntities()

----------------- Unit Testing

------------------------------------------ Love2D stuffs
LoveSprites = {}
function CompEqual(comp1, comp2)
	if #comp1 ~= #comp2 then return false end 
	for i = 1, #comp1 do
		if comp1[i] ~= comp2[i] then 
			return false 
		end 
	end 
	return true
end 
function CompEqualSorted(comp1, comp2)
	table.sort(comp1)
	table.sort(comp2)
	return CompEqual(comp1,comp2)
end 

local lovetest = require "test/lovetest"

function love.load()
	-- testing flag
	if lovetest.detect(arg) then 
		lovetest.run()
	end 
	love.graphics.setDefaultFilter("nearest", "nearest")
	_init()
	table.insert(LoveSprites, love.graphics.newImage("santa.png"))
end 

function love.keyreleased(key)
   if key == "escape" then
      love.event.quit()
   end
end

function love.update(dt)
	_update60()
end

function love.draw()
	love.graphics.print("Hello World!", 400, 300)
	love.graphics.draw(LoveSprites[1],500,200,0,4,4)
	_draw()
end

function btn(key)
	return false 
end 
