pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
ecsentityid = 1
ecsbucketid = 1
ecsentities = {}
ecscomponents = {}
ecsusystems = {}
ecsdsystems = {}
ecsbucketslist = {}
ecsbuckets = {} 

function ecsnextentityid()
	ecsentityid = ecsentityid + 1
	return ecsentityid - 1
end

function ecsnextbucketid()
	ecsbucketid = ecsbucketid + 1
	return ecsbucketid - 1
end 

-- returns existing bucket or new one
function ecsgetbucket(comps_list)
	local i, bl, found 
	local buckid = nil
	for i, c in pairs(ecsbucketslist) do 
		if ecscompeq(comps_list, c) then 
			buckid = i
			break
		end 
	end 
	if buckid == nil then 
		buckid = ecsnextbucketid()
		ecsbuckets[buckid] = {}
		ecsbucketslist[buckid] = comps_list
	end
	return ecsbuckets[buckid]
end

-- get all compatible buckets to passed components list
function ecsgetcompatbuckets(comps_list)
	local buckids = {}
	for i, c in pairs(ecsbucketslist) do 
		-- entity components (comps_list) must provide what bucket needs (c)
		-- entity "pos"  incompatible with bucket "pos", "sprite"
		if ecscompin(c, comps_list) then 
			add(buckids, i) 
		end
	end 
	return buckids
end 

-- compare two component lists for exact order match (does not work for different order)
function ecscompeqord(comp1, comp2)
	if #comp1 ~= #comp2 then return false end 
	for i = 1, #comp1 do
		if comp1[i] ~= comp2[i] then 
			return false 
		end 
	end 
	return true
end 

-- compare two component lists for unordered match (more usable and expensive)
function ecscompeq(comp1, comp2)
	if #comp1 ~= #comp2 then return false end 
	local found
	for i = 1, #comp1 do 
		found = false 
		for j = 1, #comp2 do 
			if comp1[i] == comp2[j] then 
				found = true
				break
			end
		end
		if found == false then return false end
	end 
	return true
end

-- returns true if given component name is already defined as a component or not
function ecscheckcomp(comp)
	for k, v in pairs(ecscomponents) do 
		if comp == k then 
			return true 
		end
	end
	return false
end

-- shows an error indicating that given comp is not defined
function ecsassertcomps(comps)
	local i, comp
	for i = 1, #comps do  
		comp = comps[i]
		if ecscheckcomp(comp) == false then
			local msg = "component \"" .. comp .. "\" is not defined!"
			print(msg)
		end
	end
end

function ecscreatecomp(comp)
	local newcomp = {}
	for i, v in pairs(comp) do 
		newcomp[i] = v 
	end
	return newcomp
end 

-- return true if subset is in set (bit expensive)
function ecscompin(subset, set)
	if #subset > #set then return false end
	local i, j, found
	found = 0
	for i = 1, #subset do 
		for j = 1, #set do 
			if subset[i] == set[j] then
				found = found + 1
				break
			end 
		end
	end
	return found == #subset
end 

function ecsexecsystem(system)
	if system == nil then return end
	local i, eid
	for i, eid in pairs(system.ent_bucket) do
		system.proc(ecsentities[eid].entity, eid)
	end
end

function ecsexecsystems(systems)
	local i, s
	for i, s in pairs(systems) do 
		ecsexecsystem(s)
	end
end

function updateecs()
	ecsexecsystems(ecsusystems) 
end 

function drawecs()
	ecsexecsystems(ecsdsystems)
end

function definecomponent(name, comp_data) 
	if comp_data == nil then 
		print("component " .. name .. " needs comp_data description!")
	end
	ecscomponents[name] = comp_data
end 

function defineupdatesystem(comps_list, system_proc)
	ecsassertcomps(comps_list)
	local bucket = ecsgetbucket(comps_list)
	add(ecsusystems, {proc = system_proc, ent_bucket = bucket})
end 

function definedrawsystem(comps_list, system_proc)
	ecsassertcomps(comps_list)
	local bucket = ecsgetbucket(comps_list)
	add(ecsdsystems, {proc = system_proc, ent_bucket = bucket})
end

function spawnentity(comps_list)
	ecsassertcomps(comps_list)
	local eid = ecsnextentityid()
	local entity_data = {} 
	-- create actual components in entity_data 
	local i
	for i = 1, #comps_list do
		entity_data[comps_list[i]] = ecscreatecomp(ecscomponents[comps_list[i]])
	end 
	ecsentities[eid] = {
		comps = comps_list,
		entity = entity_data
	}
	local cbucks = ecsgetcompatbuckets(comps_list)
	for i = 1, #cbucks do
		add(ecsbuckets[cbucks[i]], eid)
	end 
	return eid,entity_data
end 

function getentity(eid)
	return ecsentities[eid].entity
end

function killentity(eid)
	if ecsentities == nil or ecsentities[eid] == nil then return end
	local i, bs, j, b
	for i, bs in pairs(ecsbuckets) do
		for j, b in pairs(bs) do 
			if eid == b then 
				del(bs, eid)
				break
			end
		end
	end
	del(ecsentities, eid)
end

function killallentities()
	-- resets all entity buckets, and entities 
	ecsbuckets = {} 
	ecsbucketslist = {} 
	ecsentities = {}
end 

local cpos = {x=0,y=0}
local cplayer = {mode="play"}
local csprite = {n=1,fx=false,fy=false}
local crect8 = {c=9}
local cenemy = {t=1,s=0.5}
local cespawn = {f=0.9,a=0}

-- update systems
---- pos, player
function usplayercontrol(ent)
 local inc=0.5
 if btn(⬆️) then ent.pos.y -= inc end
 if btn(⬇️) then ent.pos.y += inc end
 if btn(➡️) then ent.pos.x += inc end
 if btn(⬅️) then ent.pos.x -= inc end
end
---- pos, enemy
function usenemycontrol(ent,id)
 ent.pos.y += ent.enemy.s
end
---- pos, enemy
function usscreenoutdeath(ent,id)
 if ent.pos.y >= 128 then
 	killentity(id)
 end
end
---- pos, player
function uskeepinscreen(ent)
 local mn = 0
 local mx = 120
	if ent.pos.x < mn then ent.pos.x = mn end
	if ent.pos.x > mx then ent.pos.x = mx end
	if ent.pos.y < mn then ent.pos.y = mn end
	if ent.pos.y > mx then ent.pos.y = mx end	
end
---- espawn
function usspawnenemies(ent)
 ent.espawn.a+=ent.espawn.f/60
 if ent.espawn.a >= 1 then
  ent.espawn.a-=1
  ent.espawn.f=0.8+rnd(1)
  local id,e=spawnentity({"pos","sprite","enemy"})
  e.pos.x=8+(rnd(1)*112)
  e.pos.y=-7
  e.sprite.n=3
 end
end

-- draw systems
---- pos, rect8
function dsrect8(ent)
	rectfill(ent.pos.x,ent.pos.y,ent.pos.x+8,ent.pos.y+8,ent.rect8.c)
end
---- pos, sprite
function dssprite(ent)
 spr(ent.sprite.n,ent.pos.x,ent.pos.y)
end

function _init()
 cls()
 print("♥",0,0,14)
 -- components
 definecomponent("pos",cpos)
 definecomponent("player",cplayer)
 definecomponent("enemy",cenemy)
 definecomponent("sprite",csprite)
 definecomponent("rect8",crect8)
 definecomponent("espawn",cespawn)
 -- systems
 defineupdatesystem({"pos","player"},usplayercontrol)
 defineupdatesystem({"pos","enemy"},usenemycontrol)
 defineupdatesystem({"pos","enemy"},usscreenoutdeath)
 defineupdatesystem({"pos","player"},uskeepinscreen)
 defineupdatesystem({"espawn"},usspawnenemies)
 definedrawsystem({"pos","rect8"},dsrect8)
 definedrawsystem({"pos","sprite"},dssprite)
 -- entities 
 ---- player
 local eid=spawnentity({"pos","sprite","player"})
 local ed=getentity(eid)
 ed.pos.x=60
 ed.pos.y=50
 ed.sprite.n=2
 ---- enemy spawner
 eid,ed=spawnentity({"espawn"})
 ed.a=1
end

function _update60()
	updateecs()
end

function _draw()
	cls()
	drawecs()
end

__gfx__
00000000070000700005500000555500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000777007770555555055555555000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00700700077777700099990000899800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00077000077777700008800000999900000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00077000078778700cccccc00a1a1a10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0070070007777770c1cccc1ca0a1a101000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000578875090888809101a1a0a000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000057777500080080000400400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
