local inspect = require('inspect')

local ents_list = {
    { "pos", "player"},
    { "sprite", "pos", "player" },
    { "pos" }
}

local tempout = ""

tUSPlayerController = function(ent)
	--if btn(1) then ent.pos.x = ent.pos.x + 1 end 
    --if btn(0) then ent.pos.x = ent.pos.x - 1 end 
    tempout = "USPC"
end 

tDSSpriteDrawer = function(ent)
    --spr(ent.sprite.frame, ent.pos.x, ent.pos.y, 1, 1, ent.sprite.flip_x, ent.sprite.flip_y)
    --print("RUNN_SPRITE")
    tempout = "DS1"
end 

tDSEnemyDrawer = function(ent) 
    --print("RUNN_ENEMY")
    tempout = "DS2"
end

local ent0 = nil 
local ent1 = nil
local ent2 = nil

function sizeof(t) 
    local count = 0
    for k, v in pairs(t) do 
        if v ~= nil then 
            count = count + 1
        end
    end
    return count
end

function fullInspection()
    print("\nBEGIN FULL INSPECTION")
    print("-- ecsEntityId: " .. ecsEntityId)
    print("-- ecsBucketId: " .. ecsBucketId)
    print("-- ecsEntities: \n" .. inspect(ecsEntities))
    print("-- ecsComponents: \n" .. inspect(ecsComponents))
    print("-- ecsUSystems: \n" .. inspect(ecsUSystems))
    print("-- ecsDSystems: \n" .. inspect(ecsDSystems))
    print("-- ecsBucketsList: \n" .. inspect(ecsBucketsList))
    print("-- ecsBuckets: \n" .. inspect(ecsBuckets))
    print("END FULL INSPECTION\n")
end

function setup_testing_scene() 
    ecsHardReset()
    tempout = ""
    local tCPos = {
        x = 0,
        y = 0,
    }    
    local tCSprite = {
        frame = 1,
        flip_x = false,
        flip_y = false
    }    
    local tCPlayer = {

        mode = "play"
    }
    DefineComponent("pos", tCPos)
    DefineComponent("sprite", tCSprite)
    DefineComponent("player", tCPlayer)
    DefineUpdateSystem({"pos", "player"}, tUSPlayerController)
    DefineDrawSystem({"sprite"}, tDSSpriteDrawer)
    DefineDrawSystem({"pos", "sprite"}, tDSEnemyDrawer)
    ent0 = SpawnEntity({"pos", "sprite", "player"})
    ent1 = SpawnEntity({"pos"})
    ent2 = SpawnEntity({"pos", "sprite"})
end

function setup_testing_scene_2()
    ecsHardReset()
    tempout = ""
    local tCPos = {
        x = 0,
        y = 0,
    }    
    local tCSprite = {
        frame = 1,
        flip_x = false,
        flip_y = false
    }    
    local tCPlayer = {
        mode = "play"
    }
    DefineComponent("pos", tCPos)
    DefineComponent("sprite", tCSprite)
    DefineUpdateSystem({"pos"}, tUSPlayerController)
    DefineDrawSystem({"pos", "sprite"}, tDSSpriteDrawer)
    SpawnEntity({"pos"})
    SpawnEntity({"sprite"})
    SpawnEntity({"pos", "sprite"})
    fullInspection()
end


function test_NextEntityId()
    ecsHardReset()
    local a = ecsNextEntityId()
    local b = ecsNextEntityId()
    assert_equal(a == 1, true)
    assert_equal(b == 2, true)
end

function test_NextBucketId()
    ecsHardReset()
    local a = ecsNextBucketId()
    local b = ecsNextBucketId()
    assert_equal(a == 1, true)
    assert_equal(b == 2, true)
end

function test_GetBucket()
    ecsHardReset()
    assert_len(0, ecsBucketsList)
    assert_len(0, ecsBuckets)
    local bucket = ecsGetBucket(ents_list[1])
    assert_len(0, bucket)
    assert_len(1, ecsBucketsList)
    assert_len(1, ecsBuckets)
    assert_not_nil(ecsBuckets[1])
    assert_not_nil(ecsBucketsList[1])
    assert_true(CompEqual(ecsBucketsList[1], ents_list[1]))
    local re_bucket = ecsGetBucket(ents_list[1])
    assert_len(0, bucket)
    assert_len(1, ecsBucketsList)
    assert_len(1, ecsBuckets)
end

function test_GetCompatBuckets()
    setup_testing_scene()
    local b1 = ecsGetCompatBuckets(ents_list[1])
    local b2 = ecsGetCompatBuckets(ents_list[2])
    local b3 = ecsGetCompatBuckets(ents_list[3])
    assert_table(b1)
    assert_len(1, b1)
    assert_len(3, b2)
    assert_len(0, b3)
end

function test_ecsCompEqOrd()
    local c1 = {"pos", "player"}
    local c2 = {"pos", "sprite"}
    local c3 = {"pos", "player"}
    assert_false(ecsCompEqOrd(c1, c2))
    assert_true(ecsCompEqOrd(c1, c3))
end

function test_ecsCompEq()
    local c1 = {"pos", "player"}
    local c2 = {"pos", "sprite"}
    local c3 = {"pos", "player"}
    local c4 = {"player", "pos"}
    assert_false(ecsCompEq(c1, c2))
    assert_true(ecsCompEq(c1, c3))
    assert_true(ecsCompEq(c1, c4))
end

function test_ecsCheckComp()
    setup_testing_scene()
    assert_true(ecsCheckComp("pos"))
    assert_true(ecsCheckComp("sprite"))
    assert_false(ecsCheckComp("man"))
    --ecsAssertComp("man") <- works
end

function test_CreateComp()
    setup_testing_scene()
    local nc = ecsCreateComp(ents_list[1])
    assert_len(2, nc)
    assert_equal(nc[1], "pos")
    assert_equal(nc[2], "player")
    assert_not_equal(nc, ents_list[1])
end

function test_CompIn()
    setup_testing_scene()
    assert_true(ecsCompIn({"player"}, ents_list[1]))
    assert_true(ecsCompIn({"sprite", "pos"}, ents_list[2]))
    assert_true(ecsCompIn({"pos", "player"}, ents_list[1]))
    assert_false(ecsCompIn({"player"}, ents_list[3]))
end

function test_ExecSystem()
    --setup_testing_scene_2()
    setup_testing_scene()
    assert_table(ecsUSystems)
    assert_table(ecsUSystems[1])
    ecsExecSystem(ecsUSystems[1])
    assert_equal("USPC", tempout)
    tempout = "blah"
    assert_len(2, ecsDSystems)
    ecsExecSystem(ecsDSystems[1])
    assert_equal("DS1", tempout)
end

function test_ExecSystems()
    setup_testing_scene()
    ecsExecSystems(ecsUSystems)
    assert_equal(tempout, "USPC")
    ecsExecSystems(ecsDSystems)
    assert_equal(tempout, "DS2")
end

function test_UpdateECS()
    -- no need
end

function test_DrawECS()
    -- no need
end

function test_DefineComponent()
    setup_testing_scene()
    assert_equal(sizeof(ecsComponents), 3)
    DefineComponent("blah", {ma = 42, da = 1337})
    assert_equal(sizeof(ecsComponents), 4)
    assert_table(ecsComponents["blah"])
    assert_equal(ecsComponents["blah"].da, 1337)
end

function test_DefineUpdateSystem()
    setup_testing_scene()
    local sc = sizeof(ecsUSystems)
    DefineUpdateSystem({"blah"}, test_DrawECS)
    assert_equal(sizeof(ecsUSystems), sc + 1)
end

function test_DefineDrawSystem()
    setup_testing_scene()
    local sc = sizeof(ecsDSystems)
    DefineDrawSystem({"blah"}, test_DrawECS)
    assert_equal(sizeof(ecsDSystems), sc + 1)
end

function test_SpawnEntity()
    setup_testing_scene()
    local ce = sizeof(ecsEntities)
    local be = sizeof(ecsBuckets)
    local e1 = SpawnEntity({"pos", "player"})
    local e2 = SpawnEntity({"sprite"})
    assert_equal(ce + 2, sizeof(ecsEntities))
    assert_not_nil(ecsEntities[e1])
    assert_table(ecsEntities[e1])
    assert_equal(ecsEntities[e1].comps[1], "pos")
    assert_table(ecsEntities[e1].entity)
    assert_equal(ecsEntities[e1].entity.pos.x, 0)
    assert_equal(ecsEntities[e1].entity.player.mode, "play")
end

function test_KillEntity()
    setup_testing_scene()
    local ee = sizeof(ecsEntities)
    KillEntity(ent1)
    assert_lt(ee, sizeof(ecsEntities))
    KillEntity(ent0)
    assert_equal(1, sizeof(ecsEntities))
end

function test_KillAllEntities()
    setup_testing_scene()
    KillAllEntities()
    assert_equal(0, sizeof(ecsEntities))
end

