local val = {"pos", "player"}
local dicts = {
    {"pos", "player"},
    {"player", "pos"}
}

--[[
-- doesn't work!
function test_directComparison()
    assert_true(val == dicts[1])
end]]--

function test_simpleCompare()
    assert_true(CompEqual(val, dicts[1]))
end

function test_sortedCompare()
    assert_true(CompEqualSorted, dicts[1])
    assert_true(CompEqualSorted, dicts[2])
end
